// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.subsystems.SwerveSubsystem;
import static frc.robot.Constants.DriveConstants.*;
import static frc.robot.Constants.OIConstants.*;
import static java.lang.Math.*;

import java.util.function.Supplier;

public class SwerveJoystickCommand extends CommandBase {
  /** Creates a new SwerveJoystickCommand. */
    private final SwerveSubsystem swerveSubsystem;
    private final Supplier<Double> xSpeedFunction;
    private final Supplier<Double> ySpeedFunction;
    private final Supplier<Double> turnSpeedFunction;
    private final Supplier<Boolean> fieldOrientedFunction;
    // TODO rate limiters

  public SwerveJoystickCommand(SwerveSubsystem swerveSubsystem,
      Supplier<Double> xSpeedFunction, Supplier<Double> ySpeedFunction,
      Supplier<Double> turnSpeedFunction, Supplier<Boolean> fieldOrientedFunction) {
    this.swerveSubsystem = swerveSubsystem;
    this.xSpeedFunction = xSpeedFunction;
    this.ySpeedFunction = ySpeedFunction;
    this.turnSpeedFunction = turnSpeedFunction;
    this.fieldOrientedFunction = fieldOrientedFunction;
    addRequirements(swerveSubsystem);
  }

  @Override
  public void initialize() {
    swerveSubsystem.brakeMode();
  }
  
  @Override
  public void execute() {
    // get real-time joystick inputs
    double xSpeed = xSpeedFunction.get();
    double ySpeed = ySpeedFunction.get();
    double turnSpeed = turnSpeedFunction.get();

    // apply deadband
    // if (abs(xSpeed) < AXIS_DEADBAND) xSpeed = 0.0;
    // if (abs(ySpeed) < AXIS_DEADBAND) ySpeed = 0.0;
    // if (abs(turnSpeed) < TURN_DEADBAND) turnSpeed = 0.0;

    // convert to meters per second
    xSpeed *= MAX_DRIVE_SPEED;
    ySpeed *= MAX_DRIVE_SPEED;
    turnSpeed *= MAX_TURN_SPEED;

    // construct desired chassis speeds
    ChassisSpeeds chassisSpeeds;
    // if (fieldOrientedFunction.get().equals(Boolean.TRUE)) {
    //   // relative to field
    //   chassisSpeeds = ChassisSpeeds.fromFieldRelativeSpeeds(
    //       ySpeed, xSpeed, turnSpeed, swerveSubsystem.getGyroRotation2d());
    // } else {
      // relative to robot
      chassisSpeeds = new ChassisSpeeds(ySpeed, xSpeed, turnSpeed);
    // }

    // convert chassis speeds to individual module states
    SwerveModuleState[] moduleStates = DRIVE_KINEMATICS.toSwerveModuleStates(chassisSpeeds);
    SmartDashboard.putString("chassisSpeeds", chassisSpeeds.toString());
    SmartDashboard.putNumber("speed", hypot(chassisSpeeds.omegaRadiansPerSecond*TRACK_WIDTH/2,chassisSpeeds.omegaRadiansPerSecond*WHEEL_BASE/2));
    // output module states to each wheel
    swerveSubsystem.setModuleStates(moduleStates);
  }
  
  @Override
  public void end(boolean interrupted) {
    swerveSubsystem.stopMotors();
    swerveSubsystem.coastMode();
  }
  
  @Override
  public boolean isFinished() {
    return false;
  }

}
