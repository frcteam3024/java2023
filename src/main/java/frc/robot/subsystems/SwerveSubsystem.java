package frc.robot.subsystems;

// import com.kauailabs.navx.frc.AHRS;

// import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.SwerveModuleState;
// import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.SwerveModule;
import static frc.robot.Constants.DriveConstants.*;

public class SwerveSubsystem extends SubsystemBase {

  private final SwerveModule frontLeftModule  = new SwerveModule(
    FL_DRIVE_MOTOR_PORT,
    FL_TURN_MOTOR_PORT,
    FL_TURN_ENCODER_PORT,
    FL_DRIVE_MOTOR_REVERSED,
    FL_TURN_MOTOR_REVERSED,
    FL_TURN_ENCODER_REVERSED,
    FL_TURN_OFFSET_RAD
  );
  private final SwerveModule frontRightModule  = new SwerveModule(
    FR_DRIVE_MOTOR_PORT,
    FR_TURN_MOTOR_PORT,
    FR_TURN_ENCODER_PORT,
    FR_DRIVE_MOTOR_REVERSED,
    FR_TURN_MOTOR_REVERSED,
    FR_TURN_ENCODER_REVERSED,
    FR_TURN_OFFSET_RAD
  );
  private final SwerveModule backLeftModule  = new SwerveModule(
    BL_DRIVE_MOTOR_PORT,
    BL_TURN_MOTOR_PORT,
    BL_TURN_ENCODER_PORT,
    BL_DRIVE_MOTOR_REVERSED,
    BL_TURN_MOTOR_REVERSED,
    BL_TURN_ENCODER_REVERSED,
    BL_TURN_OFFSET_RAD
  );
  private final SwerveModule backRightModule  = new SwerveModule(
    BR_DRIVE_MOTOR_PORT,
    BR_TURN_MOTOR_PORT,
    BR_TURN_ENCODER_PORT,
    BR_DRIVE_MOTOR_REVERSED,
    BR_TURN_MOTOR_REVERSED,
    BR_TURN_ENCODER_REVERSED,
    BR_TURN_OFFSET_RAD
  );

  private final SwerveModule[] allSwerveModules = {
    frontLeftModule,
    frontRightModule,
    backLeftModule,
    backRightModule
  };

  // private final AHRS gyro = new AHRS(SPI.Port.kMXP);

  public SwerveSubsystem() {
    // allow 1 sec for gyro to boot up. on a separate thread so other
    // functions can continue to run
    // new Thread(() -> {
    //   try {
    //     Thread.sleep(1000);
    //     zeroHeading();
    //   } catch (InterruptedException e) {
    //     e.printStackTrace();
    //     Thread.currentThread().interrupt();
    //   }
    // }).start();
  }

  // public void zeroHeading() {
  //   gyro.reset();
  // }

  // public double getGyroDegrees() {
  //   // range (-180, 180]
  //   return Math.IEEEremainder(gyro.getAngle(), 360);
  // }

  // public Rotation2d getGyroRotation2d() {
  //   return Rotation2d.fromDegrees(getGyroDegrees());
  // }

  public void brakeMode() {
    for(SwerveModule module : allSwerveModules) {
      module.brakeMode();
    }
   }

  public void coastMode(){
    for(SwerveModule module : allSwerveModules){
      module.coastMode();
    }
  }

  public void stopMotors(){
    for(SwerveModule module : allSwerveModules){
      module.stopMotors();
    }
  }

  public void setModuleStates(SwerveModuleState[] desiredStates) {
    for (int i = 0; i < 4; i++){
      SwerveModuleState desiredState = desiredStates[i];
      allSwerveModules[i].setModuleState(desiredState);
    }
  }
}
