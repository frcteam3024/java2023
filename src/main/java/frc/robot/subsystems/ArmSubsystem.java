// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import static frc.robot.Constants.ArmConstants.*;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class ArmSubsystem extends SubsystemBase {
  
  // local vars for all three motor
  private final CANSparkMax glenohumeralMotor = new CANSparkMax(GLENOHUMERAL_MOTOR_ID, MotorType.kBrushless);
  private final CANSparkMax ulnohumeralMotor = new CANSparkMax(ULNOHUMERAL_MOTOR_ID, MotorType.kBrushless);
  private final CANSparkMax radiocarpalMotor = new CANSparkMax(RADIOCARPAL_MOTOR_ID, MotorType.kBrushless);

  /*
   *  create two encoders (class Encoder), one for glenohumeral and one for ulnohumeral
   *  create two pid controllers (class PIDController), one for each of the above encoders
   */

  private final CANSparkMax[] allArmMotors = {
    glenohumeralMotor,
    ulnohumeralMotor,
    radiocarpalMotor
  };

  /** Creates a new ArmSubsystem. */
  public ArmSubsystem() {
  
  }

  // methods to set each motor to a given speed
  public void setGlenohumeral(double speed) {
    glenohumeralMotor.set(speed);
  }
 
  public void setUlnohumeral(double speed) {
    ulnohumeralMotor.set(speed);
  }

  public void setRadiocarpal(double speed) {
    radiocarpalMotor.set(speed);
  }

  public void stopMotors() {
      for(CANSparkMax motor : allArmMotors) {
        motor.stopMotor();
      } 
  }

  public void brakeMode() {
      for(CANSparkMax motor : allArmMotors) {
        motor.setIdleMode(IdleMode.kBrake);
      }
  }

  public void coastMode() {
    for(CANSparkMax motor : allArmMotors) {
        motor.setIdleMode(IdleMode.kCoast);
    }
  }
}

