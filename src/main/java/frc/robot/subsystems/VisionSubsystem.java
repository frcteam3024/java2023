package frc.robot.subsystems;

import edu.wpi.first.apriltag.AprilTag;
import edu.wpi.first.apriltag.AprilTagFieldLayout;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Pose3d;
import edu.wpi.first.math.geometry.Transform3d;

import static frc.robot.Constants.FieldConstants.*;
import static frc.robot.Constants.VisionConstants.*;
import static java.lang.Math.*;
import java.util.ArrayList;
import org.photonvision.PhotonCamera;
import org.photonvision.PhotonPoseEstimator;
import org.photonvision.PhotonUtils;
import org.photonvision.PhotonPoseEstimator.PoseStrategy;
import org.photonvision.targeting.PhotonTrackedTarget;

public class VisionSubsystem {
  public static PhotonCamera camera;
  public static PhotonPoseEstimator cameraPoseEstimator;

  public static void update(String team) {
    AprilTag tagSupply;
    AprilTag tagDeploymentLeft;
    AprilTag tagDeploymentCenter;
    AprilTag tagDeploymentRight;

    if (team.equals("red")) {
      tagSupply = new AprilTag(4, new Pose3d(TAG_SUPPLY_RED));
      tagDeploymentLeft = new AprilTag(3, new Pose3d(TAG_SUPPLY_RED));
      tagDeploymentCenter = new AprilTag(2, new Pose3d(TAG_SUPPLY_RED));
      tagDeploymentRight = new AprilTag(1, new Pose3d(TAG_SUPPLY_RED));
    } else if (team.equals("blue")) {
      tagSupply = new AprilTag(5, new Pose3d(TAG_SUPPLY_RED));
      tagDeploymentLeft = new AprilTag(6, new Pose3d(TAG_SUPPLY_RED));
      tagDeploymentCenter = new AprilTag(7, new Pose3d(TAG_SUPPLY_RED));
      tagDeploymentRight = new AprilTag(8, new Pose3d(TAG_SUPPLY_RED));
    } else {
      tagSupply = null;
      tagDeploymentLeft = null;
      tagDeploymentCenter = null;
      tagDeploymentRight = null;
    }
      
    ArrayList<AprilTag> apriltagList = new ArrayList<>();
    apriltagList.add(tagSupply);
    apriltagList.add(tagDeploymentLeft);
    apriltagList.add(tagDeploymentCenter);
    apriltagList.add(tagDeploymentRight);

    AprilTagFieldLayout aftl = 
      new AprilTagFieldLayout(apriltagList, LENGTH, WIDTH);

    camera = new PhotonCamera(CAMERA_NAME); 
    // FIXME: the camera is named chris pratt as a placeholder, change to charlie day 
    cameraPoseEstimator =
      new PhotonPoseEstimator(
        aftl, 
        PoseStrategy.CLOSEST_TO_REFERENCE_POSE, 
        camera,
        ROBOT_TO_CAM
      );
  }

  public static int apriltagID() {
    var result = camera.getLatestResult();
    PhotonTrackedTarget target = result.getBestTarget();
    return target.getFiducialId();
  }

  public static Transform3d apriltagCameraDiff() {
    var result = camera.getLatestResult();
    PhotonTrackedTarget target = result.getBestTarget();
    return target.getBestCameraToTarget();
  }

  public static double apriltagCameraDiffX() {
    var diff = apriltagCameraDiff();
    return diff.getX();
  }

  public static double apriltagCameraDiffY() {
    var diff = apriltagCameraDiff();
    return diff.getY();
  }
  
  public static double apriltagCameraDiffZ() {
    var diff = apriltagCameraDiff();
    return diff.getZ();
  }

  public static double distanceToTag() {
    // change if the camera is not the origin
    var diff = apriltagCameraDiff();
    return hypot(diff.getX(), diff.getY());
  }
}
