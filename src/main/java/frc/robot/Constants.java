// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import static java.lang.Math.PI;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
// Imported for Vision.
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.math.geometry.Translation3d;


/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
  private Constants() {}

  public static final class ModuleConstants {
    private ModuleConstants() {}

    public static final double WHEEL_DIAMETER                      = .105;
    public static final double DRIVE_MOTOR_GEAR_RATIO              = 1.0/4.0;    // ?? Davis' guess
    public static final double DRIVE_ENCODER_ROTATION_TO_METER     = DRIVE_MOTOR_GEAR_RATIO * PI * WHEEL_DIAMETER;
    public static final double DRIVE_ENCODER_RPM_TO_METERS_PER_SEC = DRIVE_ENCODER_ROTATION_TO_METER / 60.0;
    public static final double KP_TURN                             = 0.25;
    public static final double KI_TURN                             = 0.1;
    public static final double KD_TURN                             = 0;

  }

  public static final class DriveConstants {
    private DriveConstants() {}

    // distance between front and back wheels (meters)
    public static final double WHEEL_BASE  = Units.inchesToMeters(30);
    // distance between left and right wheels (meters)
    public static final double TRACK_WIDTH = Units.inchesToMeters(24);
    public static final SwerveDriveKinematics DRIVE_KINEMATICS = new SwerveDriveKinematics(
      // each argument is the location of a swerve module relative to the robot's center
      new Translation2d( WHEEL_BASE / 2, -TRACK_WIDTH / 2),
      new Translation2d( WHEEL_BASE / 2,  TRACK_WIDTH / 2),
      new Translation2d(-WHEEL_BASE / 2, -TRACK_WIDTH / 2),
      new Translation2d(-WHEEL_BASE / 2,  TRACK_WIDTH / 2)
    );

    public static final int     FL_DRIVE_MOTOR_PORT       = 14;
    public static final int     FL_TURN_MOTOR_PORT        = 7;
    public static final int     FL_TURN_ENCODER_PORT      = 3;
    public static final boolean FL_DRIVE_MOTOR_REVERSED   = true;
    public static final boolean FL_TURN_MOTOR_REVERSED    = false;
    public static final boolean FL_TURN_ENCODER_REVERSED  = false;
    public static final double  FL_TURN_OFFSET_RAD        = 0.684;

    public static final int     FR_DRIVE_MOTOR_PORT       = 13;
    public static final int     FR_TURN_MOTOR_PORT        = 6;
    public static final int     FR_TURN_ENCODER_PORT      = 2;
    public static final boolean FR_DRIVE_MOTOR_REVERSED   = false;
    public static final boolean FR_TURN_MOTOR_REVERSED    = false;
    public static final boolean FR_TURN_ENCODER_REVERSED  = false;
    public static final double  FR_TURN_OFFSET_RAD        = 3.021;

    public static final int     BL_DRIVE_MOTOR_PORT       = 12;
    public static final int     BL_TURN_MOTOR_PORT        = 5;
    public static final int     BL_TURN_ENCODER_PORT      = 1;
    public static final boolean BL_DRIVE_MOTOR_REVERSED   = true;
    public static final boolean BL_TURN_MOTOR_REVERSED    = false;
    public static final boolean BL_TURN_ENCODER_REVERSED  = false;
    public static final double  BL_TURN_OFFSET_RAD        = 2.407;

    public static final int     BR_DRIVE_MOTOR_PORT       = 11;
    public static final int     BR_TURN_MOTOR_PORT        = 4;
    public static final int     BR_TURN_ENCODER_PORT      = 0;
    public static final boolean BR_DRIVE_MOTOR_REVERSED   = false;
    public static final boolean BR_TURN_MOTOR_REVERSED    = false;
    public static final boolean BR_TURN_ENCODER_REVERSED  = false;
    public static final double  BR_TURN_OFFSET_RAD        = -1.288;

    public static final double MAX_PHYSICAL_SPEED = 2;      // meters per sec
    public static final double MAX_DRIVE_SPEED    = 2;      // meters per sec
    public static final double MAX_DRIVE_ACCEL    = 1;      // meters per sec^2
    public static final double MAX_TURN_SPEED     = 2 * PI; // radains per sec
    public static final double MAX_TURN_ACCEL     = PI;     // radians per sec^2
  }
  
  public static final class IntakeConstants {
    private IntakeConstants() {}

    public static final int INTAKE_MOTOR_ID = 8;
    public static final double MAX_INTAKE_SPEED = 1;
  }
  public static final class OIConstants {
    OIConstants() {}

    public static final double AXIS_DEADBAND          = 0.05;
    public static final double TURN_DEADBAND          = 0.20;

    public static final int DRIVE_JOYSTICK_PORT       = 0;
    public static final int COPILOT_JOYSTICK_PORT     = 2;
              
    public static final int DRIVE_X_AXIS              = 0;
    public static final int DRIVE_Y_AXIS              = 1;
    public static final int DRIVE_ROTATE_AXIS         = 2;
    public static final int DRIVE_SLIDER              = 3;

    public static final int COPILOT_LEFT_X_AXIS       = 0;
    public static final int COPILOT_LEFT_Y_AXIS       = 1;
    public static final int COPILOT_LEFT_TRIGGER      = 2;
    public static final int COPILOT_RIGHT_TRIGGER     = 3;
    public static final int COPILOT_RIGHT_X_AXIS      = 4;
    public static final int COPILOT_RIGHT_Y_AXIS      = 5;

    public static final int COPILOT_X                 = 1;
    public static final int COPILOT_A                 = 2;
    public static final int COPILOT_B                 = 3;
    public static final int COPILOT_Y                 = 4;
    public static final int COPILOT_LB                = 5;
    public static final int COPILOT_RB                = 6;
    public static final int COPILOT_LT                = 7;
    public static final int COPILOT_RT                = 8;
    public static final int COPILOT_BACK              = 9;
    public static final int COPILOT_START             = 10;
    public static final int COPILOT_LEFT_STICK_PRESS  = 11;
    public static final int COPILOT_RIGHT_STICK_PRESS = 12;

    public static final int FIELD_ORIENTED_TOGGLE_BUTTON = COPILOT_A;
    public static final int RESET_GYRO_BUTTON = COPILOT_B;
    public static final int SPIN_INTAKE_BUTTON = COPILOT_X;

  }

  public static final class ArmConstants {
    private ArmConstants() {}

    public static final int GLENOHUMERAL_MOTOR_ID = 0;
    public static final int ULNOHUMERAL_MOTOR_ID = 0;
    public static final int RADIOCARPAL_MOTOR_ID = 0;

    public static final double HUMEROUS_LENGTH = 0;
    public static final double ULNA_LENGTH = 0;
    public static final double GRIPPER_LENGTH = 0;

  }

  public static final class FieldConstants {
    // might invert
    public static final double LENGTH = Units.feetToMeters(26.292);
    public static final double WIDTH = Units.feetToMeters(54.271);

    // TODO: actually set to proper field position
    public static final Pose2d TAG_SUPPLY_RED = new Pose2d(Units.feetToMeters(3.896), 0, Rotation2d.fromDegrees(0));
    public static final Pose2d TAG_SUPPLY_BLUE = new Pose2d(Units.feetToMeters(3.896), 0, Rotation2d.fromDegrees(0));

    public static final Pose2d TAG_DEPLOYMENT_RED_1 = new Pose2d(Units.feetToMeters(10.917), 0, Rotation2d.fromDegrees(0));
    public static final Pose2d TAG_DEPLOYMENT_RED_2 = new Pose2d(Units.feetToMeters(14.792), 0, Rotation2d.fromDegrees(0));
    public static final Pose2d TAG_DEPLOYMENT_RED_3 = new Pose2d(Units.feetToMeters(22.667), 0, Rotation2d.fromDegrees(0));
    public static final Pose2d TAG_DEPLOYMENT_BLUE_1 = new Pose2d(Units.feetToMeters(10.917), 0, Rotation2d.fromDegrees(0));
    public static final Pose2d TAG_DEPLOYMENT_BLUE_2 = new Pose2d(Units.feetToMeters(14.792), 0, Rotation2d.fromDegrees(0));
    public static final Pose2d TAG_DEPLOYMENT_BLUE_3 = new Pose2d(Units.feetToMeters(22.667), 0, Rotation2d.fromDegrees(0));
  }

  public static final class VisionConstants {
    public static final Transform3d ROBOT_TO_CAM = new Transform3d(
            new Translation3d(0, 0, 0),
            new Rotation3d(0, 0, 0)); // Cam mounted facing forward.

    public static final String CAMERA_NAME = "Chris Pratt";
  }

}
