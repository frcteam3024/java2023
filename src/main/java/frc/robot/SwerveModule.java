// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.wpilibj.AnalogEncoder;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.simulation.AnalogEncoderSim;
import edu.wpi.first.wpilibj.simulation.EncoderSim;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import static java.lang.Math.PI;
import static frc.robot.Constants.ModuleConstants.*;
import static frc.robot.Constants.DriveConstants.*;

/** Add your docs here. */
public class SwerveModule {

    private final CANSparkMax driveMotor;
    private final TalonSRX turnMotor;
    
    private final RelativeEncoder driveEncoder;
    private final AnalogEncoder turnEncoder;

    private final ProfiledPIDController turnPID;

    private final boolean turnEncoderReversed;
    private final double turnEncoderOffsetRad;

    private final AnalogEncoderSim turnEncoderSim;

    public SwerveModule(int driveMotorID, int turnMotorID, int turnEncoderID,
        boolean driveMotorReversed, boolean turnMotorReversed,
        boolean turnEncoderReversed, double turnEncoderOffsetRad) {

      this.driveMotor = new CANSparkMax(driveMotorID, CANSparkMaxLowLevel.MotorType.kBrushless);
      this.driveMotor.setInverted(driveMotorReversed);

      this.turnMotor = new TalonSRX(turnMotorID);
      this.turnMotor.setInverted(turnMotorReversed);

      this.driveEncoder = driveMotor.getEncoder();
      this.driveEncoder.setPositionConversionFactor(DRIVE_ENCODER_ROTATION_TO_METER);
      this.driveEncoder.setVelocityConversionFactor(DRIVE_ENCODER_RPM_TO_METERS_PER_SEC);

      this.turnEncoder = new AnalogEncoder(turnEncoderID);
      this.turnEncoder.setDistancePerRotation(2.0 * Math.PI);
      this.turnEncoderReversed = turnEncoderReversed;
      this.turnEncoderOffsetRad = turnEncoderOffsetRad;

      this.turnPID = new ProfiledPIDController(
        KP_TURN, KI_TURN, KD_TURN,
        new TrapezoidProfile.Constraints(
          MAX_TURN_SPEED,
          MAX_TURN_ACCEL
        )
      );
      this.turnPID.enableContinuousInput(-PI, PI);

      this.turnEncoderSim = new AnalogEncoderSim(turnEncoder);
      this.turnEncoderSim.setPosition(new Rotation2d(turnEncoderOffsetRad));
    
      resetEncoders();
    }

    public double getDrivePosition() {
      return driveEncoder.getPosition();
    }

    public double getTurnRad() {
      double angle = turnEncoder.getDistance();
      angle -= turnEncoderOffsetRad;
      angle *= (turnEncoderReversed ? -1.0 : 1.0);
      angle = Math.IEEEremainder(angle, 2*Math.PI);
      return angle;
    }
    
    public double getSimRad() {
      double angle = turnEncoderSim.getPosition().getRadians();
      angle -= turnEncoderOffsetRad;
      angle *= (turnEncoderReversed ? -1.0 : 1.0);
      angle = Math.IEEEremainder(angle, 2*Math.PI);
      return angle;
    }

    public double getDriveVelocity() {
      return driveEncoder.getVelocity();
    }

    public void resetEncoders() {
      driveEncoder.setPosition(0);
      turnEncoder.reset();
    }
    
    public SwerveModuleState getState() {
      return new SwerveModuleState(getDriveVelocity(), new Rotation2d(getTurnRad()));
    }

    public void setModuleState(SwerveModuleState desiredState) {
      SmartDashboard.putString("swerve ["+turnEncoder.getChannel()+"] state", desiredState.toString());
      // if (Math.abs(desiredState.speedMetersPerSecond) < 0.001) {
      //  stopMotors();
      //  return;
      // }
      desiredState = SwerveModuleState.optimize(desiredState, getState().angle);

      double driveOutput = desiredState.speedMetersPerSecond / MAX_PHYSICAL_SPEED;
      double desiredDegrees = Math.round(1000 * desiredState.angle.getDegrees())/1000.0;
      double currentDegrees = Math.round(1000 * getSimRad() * 180 / Math.PI)/1000.0;
      double error = Math.IEEEremainder(Math.round(1000 * (desiredDegrees - currentDegrees))/1000.0,360);
      double turnOutput = turnPID.calculate(turnEncoderSim.getPosition().getRadians(), desiredState.angle.getRadians());
      // double turnOutput = Math.round(1000 * error * KP_TURN)/1000.0;
      // driveMotor.set(driveOutput);
      // if (Math.abs(desiredState.angle.getRadians()-getTurnRad()) > 3)
      // turnMotor.set(ControlMode.PercentOutput, turnOutput);
      double simEncoderOutput = turnEncoderSim.getPosition().getRadians() + turnOutput * 1.0;
      turnEncoderSim.setPosition(new Rotation2d(simEncoderOutput));
      SmartDashboard.putNumber("swerve ["+turnEncoder.getChannel()+"] desired degrees", desiredDegrees);
      SmartDashboard.putNumber("swerve ["+turnEncoder.getChannel()+"] current degrees", currentDegrees);
      SmartDashboard.putNumber("swerve ["+turnEncoder.getChannel()+"] error", error);
      SmartDashboard.putNumber("swerve ["+turnEncoder.getChannel()+"] turn output", turnOutput);
      SmartDashboard.putNumber("swerve ["+turnEncoder.getChannel()+"] sim pos", simEncoderOutput);
    }

    public void stopMotors() {
      driveMotor.set(0);
      turnMotor.set(ControlMode.PercentOutput, 0);
      // turnEncoderSim.setPosition(new Rotation2d(turnEncoderOffsetRad));
    }

    public void brakeMode() {
      driveMotor.setIdleMode(IdleMode.kBrake);
      turnMotor.setNeutralMode(NeutralMode.Brake);
    }

    public void coastMode() {
      driveMotor.setIdleMode(IdleMode.kCoast);
      turnMotor.setNeutralMode(NeutralMode.Coast);
    }

}
